import pandas as pd
import os

mypath = os.getcwd()

csv_name = 'MuPix10_PADS.csv'

pads_df = pd.read_csv(csv_name)
#print(list(pads_df.columns.values))



# write one line with instructions to file for each footprint pad
# more information look at the LibraryFileFormats.pdf in this directory

amount_pins = len(pads_df.index)


pads_output = os.path.join(mypath, "pads.txt")
with open(pads_output, "a") as pads_out_file:
    for i in range(amount_pins):
        #pin_name = str(pads_df.Signal[i])
        pin_number = str(i+1)
        x_pos = str(round(i*0.2, 3))
        print(x_pos)
        y_pos = str(0)
        #shape = str(pads_df.Type[i])
        output_string = "  (pad" + " " + pin_number + " " + "smd rect (at "  + x_pos + " " + y_pos + ")" + " " + "(size 0.1 0.2)" + " " + "(layers B.Cu B.Paste B.Mask)" + " " + "(clearance 0.1)" + ")"
        pads_out_file.write(output_string + "\n")
