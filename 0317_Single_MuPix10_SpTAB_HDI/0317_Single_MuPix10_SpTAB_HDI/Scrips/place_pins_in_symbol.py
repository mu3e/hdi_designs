import pandas as pd
import os

mypath = os.getcwd()

csv_name = 'MuPix10_PADS.csv'

pads_df = pd.read_csv(csv_name)
print(list(pads_df.columns.values))



# write this structure to file for each schematic pin
# X name pin X Y length orientation sizenum sizename part dmg type shape
# more information look at the LibraryFileFormats.pdf in this directory

y_pos = "-300"
amount_pins = len(pads_df.index)


schematic_pins_output = os.path.join(mypath, "schematic_pins.txt")
with open(schematic_pins_output, "a") as sch_pins_out_file:
    for i in range(amount_pins):
        pin_name = str(pads_df.Signal[i])
        pin_number = str(i+1)
        x_pos = str(i*150)
        y_pos = str(0)
        shape = str(pads_df.Type[i])
        output_string = "X" + " " + pin_name + " " + pin_number + " " + x_pos + " " + y_pos + " " + "200" + " " + "D" + " " + "50" + " " + "50" + " " + "0" + " " + "0" + " " + shape
        sch_pins_out_file.write(output_string + "\n")
