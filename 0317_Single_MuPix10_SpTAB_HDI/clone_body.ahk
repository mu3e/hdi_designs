﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

; Warning  All coordiantes are hardcoded 

CoordMode, Pixel, Screen
CoordMode, Mouse, Screen

WaitTime = 5000


; click clone icon
Click, 383, 103
Sleep, 500
; chose newes body in list
Send, {End}
Sleep, 500
; Enter label
Click, 130, 850
Sleep, 500
Send, ^v 
Send, _clone
Sleep, 500
; click placement
Click, 15, 833
Sleep, 500
; Winkel
Click, 123, 851 #click field
Sleep, 500
Click, 399, 849 #click formula icon
Sleep, %WaitTime%
Send, ^v 
Send, .Placement.Rotation.Angle
Send, {Enter}
Sleep, 500
; Achsen
Click, 36, 872
Sleep, 500
; x
Click, 123, 892 #click field
Sleep, 500
Click, 399, 888 #click formula icon
Sleep, %WaitTime%
Send, ^v 
Send, .Placement.Rotation.Axis.x
Send, {Enter}
Sleep, 500
; y
Click, 123, 913 #click field
Sleep, 500
Click, 399, 905 #click formula icon
Sleep, %WaitTime%
Send, ^v 
Send, .Placement.Rotation.Axis.y
Send, {Enter}
Sleep, 500
; z
Click, 123, 928 #click field
Sleep, 500
Click, 399, 929 #click formula icon
Sleep, %WaitTime%
Send, ^v 
Send, .Placement.Rotation.Axis.z
Send, {Enter}
Sleep, 500

; Position
Click, 36, 951
Sleep, 500
; x
Click, 123, 972 #click field
Sleep, 500
Click, 399, 968 #click formula icon
Sleep, %WaitTime%
Send, ^v 
Send, .Placement.Base.x
Send, {Enter}
Sleep, 500
; y
Click, 123, 991 #click field
Sleep, 500
Click, 399, 990 #click formula icon
Sleep, %WaitTime%
Send, ^v 
Send, .Placement.Base.y
Send, {Enter}
Sleep, 500
; z
Click, 123, 1012 #click field
Sleep, 500
Click, 399, 1009 #click formula icon
Sleep, %WaitTime%
Send, ^v 
Send, .Placement.Base.z
Send, {Enter}
Sleep, 500

MsgBox, 0, Clone Job, Done Cloning, 2

return